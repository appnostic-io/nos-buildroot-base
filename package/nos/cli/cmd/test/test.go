package test

import (
	"fmt"
	"github.com/spf13/cobra"

	"github.com/appnostic-io/nos/pkg/cc"
	"github.com/appnostic-io/nos/pkg/config"
)

// TestCmd returns cmd
func TestCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "test",
		Hidden:        true,
		SilenceErrors: true,
		SilenceUsage:  true,
		Short:         "Test Helper For nosticOS",
		Long:          `Test Helper For nosticOS`,
		Args:          cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			cfg, err := config.ReadConfig()
			if err != nil {
				fmt.Println(err)
				return err
			}

			return cc.ApplyNetwork(&cfg)
		},
	}

	// Add sub-commands
	cmd.AddCommand()

	return cmd
}
