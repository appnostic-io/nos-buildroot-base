package config

import (
	"github.com/spf13/cobra"

	"github.com/appnostic-io/nos/pkg/cc"
	"github.com/appnostic-io/nos/pkg/config"
)

// ConfigCmd returns cmd
func bootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "boot",
		Hidden:        true,
		SilenceErrors: true,
		SilenceUsage:  true,
		Short:         "Initialize Boot Config",
		Long:          `Initialize Boot Config`,
		Args:          cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			cfg, err := config.ReadConfig()
			if err != nil {
				return err
			}

			return cc.BootApply(&cfg)
		},
	}

	return cmd
}
