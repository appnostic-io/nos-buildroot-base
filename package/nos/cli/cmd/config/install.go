package config

import (
	"github.com/spf13/cobra"

	"github.com/appnostic-io/nos/pkg/cc"
	"github.com/appnostic-io/nos/pkg/config"
)

// ConfigCmd returns cmd
func installCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "install",
		Hidden:        true,
		SilenceErrors: true,
		SilenceUsage:  true,
		Short:         "Initialize Install Config",
		Long:          `Initialize Install Config`,
		Args:          cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			cfg, err := config.ReadConfig()
			if err != nil {
				return err
			}

			return cc.InstallApply(&cfg)
		},
	}

	return cmd
}
