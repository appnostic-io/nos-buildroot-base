package config

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	//"github.com/appnostic-io/nos/pkg/config"
)

// ConfigCmd returns cmd
func ConfigCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "config",
		Hidden:        true,
		SilenceErrors: true,
		SilenceUsage:  true,
		Short:         "Load Configuration For nosticOS",
		Long:          `Load Configuration For nosticOS`,
		Args:          cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			if err := Main(); err != nil {
				logrus.Error(err)
				return err
			}
			return nil
		},
	}

	// Add sub-commands
	cmd.AddCommand(
		initrdCmd(),
		bootCmd(),
		installCmd(),
		dumpCmd(),
	)

	return cmd
}

// Main returns error
func Main() error {
	return nil
}
