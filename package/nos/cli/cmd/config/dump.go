package config

import (
	"encoding/json"
	"os"

	"github.com/spf13/cobra"

	"github.com/appnostic-io/nos/pkg/config"
)

var Format string

// ConfigCmd returns cmd
func dumpCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "dump",
		Hidden:        true,
		SilenceErrors: true,
		SilenceUsage:  true,
		Short:         "Print Config",
		Long:          `Print Config`,
		Args:          cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			cfg, err := config.ReadConfig()
			if err != nil {
				return err
			}

			if Format == "json" {
				return json.NewEncoder(os.Stdout).Encode(&cfg)
			} else {
				return config.Write(cfg, os.Stdout)
			}

		},
	}

	cmd.Flags().StringVarP(&Format, "format", "f", "", "Specifies output format")

	return cmd
}
