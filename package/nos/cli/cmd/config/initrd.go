package config

import (
	"github.com/spf13/cobra"

	"github.com/appnostic-io/nos/pkg/cc"
	"github.com/appnostic-io/nos/pkg/config"
)

// ConfigCmd returns cmd
func initrdCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "initrd",
		Hidden:        true,
		SilenceErrors: true,
		SilenceUsage:  true,
		Short:         "Initialize Initrd Config",
		Long:          `Initialize Initrd Config`,
		Args:          cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			cfg, err := config.ReadConfig()
			if err != nil {
				return err
			}

			return cc.InitApply(&cfg)
		},
	}

	return cmd
}
