package entrypoint

import (
	"github.com/spf13/cobra"
)

// EntryCmd retuns cmd
func EntryCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:    "entrypoint",
		Hidden: true,
		Short:  "A pseudo function for program entry points",
		Long:   `A pseudo function for program entry points`,
	}

	// Add sub-commands
	cmd.AddCommand(
		InitCmd(),
	)

	return cmd
}
