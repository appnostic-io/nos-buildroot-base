package entrypoint

import (
	"os"

	"github.com/appnostic-io/nos/pkg/chroot"
	"github.com/appnostic-io/nos/pkg/switchroot"
	"github.com/docker/docker/pkg/mount"
	reaper "github.com/ramr/go-reaper"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// InitCmd returns cmd
func InitCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:    "init [cmdline...]",
		Hidden: true,
		Short:  "An entrypoint for nosticOs init",
		Long:   `An entrypoint for nosticOs init`,
		Args:   cobra.ArbitraryArgs,
		Run: func(cmd *cobra.Command, args []string) {
			Init()
		},
	}
	return cmd
}

// Init returns nothing
func Init() {
	//Start the zombie process reaper by running it in the background
	go reaper.Reap()

	chroot.DebugCmdline = "nos.debug"
	switchroot.Relocate()

	if err := mount.Mount("", "/", "none", "rw,remount"); err != nil {
		logrus.Errorf("failed to remount root as rw: %v", err)
	}

	if err := chroot.Mount("./nos/data", os.Args, os.Stdout, os.Stderr); err != nil {
		logrus.Fatalf("failed to enter root: %v", err)
	}

}
