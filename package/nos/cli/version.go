package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// VersionCmd returns cmd
func VersionCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "version",
		Short: "Print the version number of nos",
		Long:  `All software has versions. This is nos's`,
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("nos v0.0.1 -- HEAD")
		},
	}

	return cmd
}
