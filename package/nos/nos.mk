################################################################################
#
# nos
#
################################################################################

NOS_VERSION = 1.0.0
NOS_SITE = $(BR2_EXTERNAL_NOSTICOS_PATH)/package/nos
NOS_SITE_METHOD=local

NOS_LICENSE = Apache-2.0
NOS_LICENSE_FILES = LICENSE

ifeq ($(BR2_PACKAGE_NOS_INIT),y)
NOS_LINK_BINARY_INIT = YES
endif

define NOS_BUILD_CMDS
   cd $(@D) \
   && $(HOST_DIR)/bin/go mod vendor \
   && CGO_ENABLED=0 $(HOST_DIR)/bin/go build -v -ldflags "-X github.com/appnostic-io/nos/pkg/version.Version=$(NOS_VERSION) -extldflags -static -s" -mod vendor -o $(@D)/bin/nos
endef

define NOS_INSTALL_TARGET_CMDS
        $(INSTALL) -D -m 755 $(@D)/bin/nos \
	  	    $(TARGET_DIR)/sbin/nos
endef

ifeq ($(BR2_PACKAGE_NOS_INIT),y)
  define NOS_LINK_TARGET_INIT
		cd $(TARGET_DIR) \
        && ln -sf sbin/nos \
	  	init
  endef
  NOS_POST_INSTALL_TARGET_HOOKS += NOS_LINK_TARGET_INIT
endif

$(eval $(golang-package))
