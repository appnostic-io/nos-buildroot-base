package config

import (
	"fmt"
	"os"
	"strconv"
	//"github.com/appnostic-io/nos/pkg/netplan/v2"
)

// NOSConfig struct
type NOSConfig struct {
	DataSources    []string               `json:"dataSources,omitempty"`
	Services       ServiceConfig          `json:"services,omitempty"`
	Engine         DockerConfig           `json:"engine,omitempty"`
	Network        map[string]interface{} `json:"network,omitempty"`
	Modules        []string               `json:"modules,omitempty"`
	Sysctls        map[string]string      `json:"sysctls,omitempty"`
	NTPServers     []string               `json:"ntpServers,omitempty"`
	DNSNameservers []string               `json:"dnsNameservers,omitempty"`
	Wifi           []Wifi                 `json:"wifi,omitempty"`
	Password       string                 `json:"password,omitempty"`
	ServerURL      string                 `json:"serverUrl,omitempty"`
	Token          string                 `json:"token,omitempty"`
	Labels         map[string]string      `json:"labels,omitempty"`
	//	K3sArgs        []string          `json:"k3sArgs,omitempty"`
	Environment map[string]string `json:"environment,omitempty"`
	Taints      []string          `json:"taints,omitempty"`
	Install     *Install          `json:"install,omitempty"`
}

// Wifi struct
type Wifi struct {
	Name       string `json:"name,omitempty"`
	Passphrase string `json:"passphrase,omitempty"`
}

// Install struct
type Install struct {
	ForceEFI  bool   `json:"forceEfi,omitempty"`
	Device    string `json:"device,omitempty"`
	ConfigURL string `json:"configUrl,omitempty"`
	Silent    bool   `json:"silent,omitempty"`
	ISOURL    string `json:"isoUrl,omitempty"`
	PowerOff  bool   `json:"powerOff,omitempty"`
	NoFormat  bool   `json:"noFormat,omitempty"`
	Debug     bool   `json:"debug,omitempty"`
	TTY       string `json:"tty,omitempty"`
}

// CloudConfig struct
type CloudConfig struct {
	SSHAuthorizedKeys []string  `json:"sshAuthorizedKeys,omitempty"`
	WriteFiles        []File    `json:"writeFiles,omitempty"`
	Hostname          string    `json:"hostname,omitempty"`
	NOS               NOSConfig `json:"nos,omitempty"`
	Runcmd            []string  `json:"runCmd,omitempty"`
	Bootcmd           []string  `json:"bootCmd,omitempty"`
	Initcmd           []string  `json:"initCmd,omitempty"`
}

// ServiceConfig defines the system services
type ServiceConfig struct {
	DNS DNSServiceConfig `json:"dns,omitempty"`
}

// DNSServiceConfig struct
type DNSServiceConfig struct {
	InterfaceName    string   `json:"interfaceName,omitempty"`
	InterfaceAddress string   `json:"interfaceAddress,omitempty"`
	NameServers      []string `json:"nameServers,omitempty"`
}

// File struct
type File struct {
	Encoding           string `json:"encoding"`
	Content            string `json:"content"`
	Owner              string `json:"owner"`
	Path               string `json:"path"`
	RawFilePermissions string `json:"permissions"`
}

// EngineRuntime describes an OCI runtime
// From https://github.com/moby/moby/blob/master/api/types/types.go
type EngineRuntime struct {
	Path string   `json:"path"`
	Args []string `yaml:"runtimeArgs,omitempty"`
}

// EngineNetworkAddressPool is a temp struct used by DockerEngine struct
// From https://github.com/moby/moby/blob/master/api/types/types.go
type EngineNetworkAddressPool struct {
	Base string `json:"base" opt:"base"`
	Size int    `json:"size" opt:"size"`
}

// DockerConfig struct
type DockerConfig struct {
	DockerEngine
}

// DockerEngine struct
type DockerEngine struct {
	APICorsHeader       string                     `json:"apiCorsHeader,omitempty" opt:"api-cors-header"`
	Bridge              string                     `json:"bridge,omitempty" opt:"bridge"`
	BIP                 string                     `json:"bridgeIp,omitempty" opt:"bip"`
	ConfigFile          string                     `json:"configFile,omitempty" opt:"config-file"`
	Containerd          string                     `json:"containerd,omitempty" opt:"containerd"`
	ContainerdNamespace string                     `json:"containerdNamespace,omitempty" opt:"containerd-namespace"`
	DataRoot            string                     `json:"dataRoot,omitempty" opt:"data-root"`
	Debug               *bool                      `json:"debug,omitempty" opt:"debug"`
	DefaultAddressPools []EngineNetworkAddressPool `json:"defaultAddressPools,omitempty" opt:"default-address-pool" ref:"EngineNetworkAddressPool"`
	DefaultGatewayIpv4  string                     `json:"defaultGateway" opt:"default-gateway"`
	DefaultGatewayIpv6  string                     `json:"defaultGatewayV6" opt:"default-gateway-v6"`
	DNSServers          []string                   `json:"dnsServers" opt:"dns"`
	DNSOptions          []string                   `json:"dnsOptions,omitempty" opt:"dns-opt"`
	DNSSearch           []string                   `json:"dnsSearch,omitempty" opt:"dns-search"`
	ExecRoot            string                     `json:"execRoot,omitempty" opt:"exec-root"`
	ExecOptions         []string                   `json:"execOptions,omitempty" opt:"exec-opt"`
	FixedCIDRv4         string                     `json:"fixedCidr,omitempty" opt:"fixed-cidr"`
	FixedCIDRv6         string                     `json:"fixedCidrV6,omitempty" opt:"fixed-cidr-v6"`
	Host                []string                   `json:"host,omitempty" opt:"host"`
	// https://docs.docker.com/engine/reference/commandline/dockerd/ says this exists, CLI says it does not ?!
	// HostGatewayIP                net.IP                     `yaml:"host_gateway_ip,omitempty" opt:"host-gateway-ip"`
	InsecureRegistries           []string          `json:"insecureRegistries,omitempty" opt:"insecure-registry"`
	IP                           string            `json:"ip,omitempty" opt:"ip"`
	IPTables                     *bool             `json:"iptables,omitempty" opt:"iptables"`
	IP6Tables                    *bool             `json:"ip6tables,omitempty" opt:"ip6tables"`
	IPv6                         *bool             `json:"ipv6,omitempty" opt:"ipv6"`
	Labels                       []string          `json:"labels,omitempty" opt:"labels"`
	LogDriver                    string            `json:"logDriver" opt:"log-driver"`
	LogLevel                     string            `json:"logLevel,omitempty" opt:"log-level"`
	LogOptions                   map[string]string `json:"logOptions,omitempty" opt:"log-options"`
	MaxConcurrentDownloads       int               `json:"maxConcurrentDownloads,omitempty" opt:"max-concurrent-downloads"`
	MaxConcurrentUploads         int               `json:"maxConcurrentUploads,omitempty" opt:"max-concurrent-uploads"`
	MaxDownloadAttempts          int               `json:"maxDownloadAttempts,omitempty" opt:"max-download-attempts"`
	MetricsAddress               string            `json:"metricsAddress,omitempty" opt:"metrics-address"`
	MTU                          int               `json:"mtu,omitempty" opt:"mtu"`
	NetworkControlPlaneMTU       int               `json:"networkControlPlaneMtu,omitempty" opt:"network-control-plane-mtu"`
	RawLogs                      *bool             `json:"rawLogs,omitempty" opt:"raw-logs"`
	RegistryMirrors              []string          `json:"registryMirrors,omitempty" opt:"registry-mirror"`
	Rootless                     *bool             `json:"rootless,omitempty" opt:"rootless"`
	Runtimes                     []string          `json:"runtimes,omitempty" opt:"add-runtime"`
	StorageDriver                string            `json:"storageDriver" opt:"storage-driver"`
	StorageOptions               []string          `json:"storageOptions,omitempty" opt:"storage-opt"`
	SwarmDefaultAdvertiseAddress string            `json:"swarmDefaultAdvertiseAddress,omitempty" opt:"swarm-default-advertise-addr"`
	TLSEnable                    *bool             `json:"tls" opt:"tls"`
	TLSCACert                    string            `json:"tlsCaCertificate" opt:"tlscacert"`
	TLSCert                      string            `json:"tlsCertificate" opt:"tlscert"`
	TLSKey                       string            `json:"tlsKey" opt:"tlskey"`
	TLSVerify                    *bool             `json:"tlsVerify" opt:"tlsverify"`
	UserlandProxy                *bool             `json:"userlandProxy,omitempty" opt:"userland-proxy"`
	UserlandProxyPath            string            `json:"userlandProxyPath,omitempty" opt:"userland-proxy-path"`
	UsernsRemap                  string            `json:"usernsRemap,omitempty" opt:"userns-remap"`
}

// Permissions returns error
func (f *File) Permissions() (os.FileMode, error) {
	if f.RawFilePermissions == "" {
		return os.FileMode(0644), nil
	}
	// parse string representation of file mode as integer
	perm, err := strconv.ParseInt(f.RawFilePermissions, 8, 32)
	if err != nil {
		return 0, fmt.Errorf("unable to parse file permissions %q as integer", f.RawFilePermissions)
	}
	return os.FileMode(perm), nil
}
