package cc

import (
	"bytes"
	"fmt"
	"io/ioutil"

	"github.com/appnostic-io/nos/pkg/config"
)

func applyNetworkManagerDefaults() error {
	buf := &bytes.Buffer{}
	buf.WriteString("[main]\n")
	buf.WriteString("dns=default\n")
	buf.WriteString("plugins=keyfile\n")
	buf.WriteString("autoconnect-retries-default=0\n")
	buf.WriteString("\n")
	buf.WriteString("[keyfile]\n")
	buf.WriteString("unmanaged-devices=interface-name:docker*;interface-name:nos*;interface-name:br*;type:veth\n")
	buf.WriteString("\n")
	buf.WriteString("[connection]\n")
	buf.WriteString("ipv4.dhcp-timeout=2147483647\n")

	err := ioutil.WriteFile("/etc/NetworkManager/NetworkManager.conf", buf.Bytes(), 0644)
	if err != nil {
		return fmt.Errorf("failed to write /etc/NetworkManager/NetworkManager.conf: %v", err)
	}

	return nil
}

// ApplyNetwork configures the network based on a
// netplan schema
func ApplyNetwork(cfg *config.CloudConfig) error {

	return applyNetworkManagerDefaults()
}
