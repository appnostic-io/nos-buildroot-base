package cc

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/appnostic-io/nos/pkg/config"
	"github.com/appnostic-io/nos/pkg/util"
	"github.com/fatih/structs"
)

func generateEngineOptions(opts *config.DockerConfig) []string {
	optsStruct := structs.New(opts.DockerEngine)

	var optsSlice []string
	for k, v := range optsStruct.Map() {
		optTag := optsStruct.Field(k).Tag("opt")

		switch value := v.(type) {
		case string:
			if value != "" {
				optsSlice = append(optsSlice, fmt.Sprintf("--%s", optTag), value)
			}
		case int:
			// AFAIK 0 disables/negates things and there's no negative numbers in docker's config
			if value >= 1 {
				optsSlice = append(optsSlice, fmt.Sprintf("--%s %d", optTag, value))
			}
		case *bool:
			if value != nil {
				if *value {
					optsSlice = append(optsSlice, fmt.Sprintf("--%s", optTag))
				} else {
					optsSlice = append(optsSlice, fmt.Sprintf("--%s=false", optTag))
				}
			}
		case []string:
			for _, elem := range value {
				if elem != "" {
					optsSlice = append(optsSlice, fmt.Sprintf("--%s", optTag), elem)
				}
			}
		case map[string]string:
			for k, v := range value {
				if v != "" {
					optsSlice = append(optsSlice, fmt.Sprintf("--%s", optTag), fmt.Sprintf("%s=%s", k, v))
				}
			}
		case []interface{}:
			// This might not be the most elegant way :\
			for _, elem := range value {
				var ifOpts []string
				var subStruct = &structs.Struct{}

				if elem != "" {
					optsSlice = append(optsSlice, fmt.Sprintf("--%s", optTag))

					// Bit messy and error prone :(
					switch k {
					case "DefaultAddressPools":
						subStruct = structs.New(opts.DockerEngine.DefaultAddressPools[0])
					}

					for ik, iv := range elem.(map[string]interface{}) {
						optTag := subStruct.Field(ik).Tag("opt")
						switch v := iv.(type) {
						case string:
							if v != "" {
								ifOpts = append(ifOpts, fmt.Sprintf("%s=%s", optTag, iv))
							}
						case int:
							if v >= 0 {
								ifOpts = append(ifOpts, fmt.Sprintf("%s=%d", optTag, iv.(int)))
							}
						}

					}
					optsSlice = append(optsSlice, fmt.Sprintf("%s", strings.Join(ifOpts, ",")))
				}

			}
		}
	}
	return optsSlice
}

// ApplyEngine configures Docker Engine options
func ApplyEngine(cfg *config.CloudConfig) error {
	opts := generateEngineOptions(&cfg.NOS.Engine)
	//var args string

	buf := &bytes.Buffer{}
	for _, c := range opts {
		buf.WriteString(c + " ")
	}

	out := fmt.Sprintf("DOCKER_OPTS=\"" + strings.TrimSpace(util.AddSlashes(buf.String())) + "\"\n")

	err := ioutil.WriteFile("/etc/conf.d/docker", []byte(out), 0644)
	if err != nil {
		return fmt.Errorf("failed to write /etc/conf.d/docker: %v", err)
	}

	return nil
}
