package cc

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	//"sort"
	"strconv"
	"strings"

	"github.com/appnostic-io/nos/pkg/command"
	"github.com/appnostic-io/nos/pkg/config"
	"github.com/appnostic-io/nos/pkg/hostname"
	"github.com/appnostic-io/nos/pkg/mode"
	"github.com/appnostic-io/nos/pkg/module"
	"github.com/appnostic-io/nos/pkg/ssh"
	"github.com/appnostic-io/nos/pkg/sysctl"
	//"github.com/appnostic-io/nos/pkg/version"
	"github.com/appnostic-io/nos/pkg/writefile"
	"github.com/milosgajdos/tenus"
	//"github.com/sirupsen/logrus"
)

func ApplyModules(cfg *config.CloudConfig) error {
	return module.LoadModules(cfg)
}

func ApplySysctls(cfg *config.CloudConfig) error {
	return sysctl.ConfigureSysctl(cfg)
}

func ApplyHostname(cfg *config.CloudConfig) error {
	return hostname.SetHostname(cfg)
}

func ApplyPassword(cfg *config.CloudConfig) error {
	return command.SetPassword(cfg.NOS.Password)
}

func ApplyRuncmd(cfg *config.CloudConfig) error {
	return command.ExecuteCommand(cfg.Runcmd)
}

func ApplyBootcmd(cfg *config.CloudConfig) error {
	return command.ExecuteCommand(cfg.Bootcmd)
}

func ApplyInitcmd(cfg *config.CloudConfig) error {
	return command.ExecuteCommand(cfg.Initcmd)
}

func ApplyWriteFiles(cfg *config.CloudConfig) error {
	writefile.WriteFiles(cfg)
	return nil
}

func ApplySSHKeys(cfg *config.CloudConfig) error {
	return ssh.SetAuthorizedKeys(cfg, false)
}

func ApplySSHKeysWithNet(cfg *config.CloudConfig) error {
	return ssh.SetAuthorizedKeys(cfg, true)
}

/*
func ApplyK3SWithRestart(cfg *config.CloudConfig) error {
	return ApplyK3S(cfg, true, false)
}

func ApplyK3SInstall(cfg *config.CloudConfig) error {
	return ApplyK3S(cfg, true, true)
}

func ApplyK3SNoRestart(cfg *config.CloudConfig) error {
	return ApplyK3S(cfg, false, false)
}

func ApplyK3S(cfg *config.CloudConfig, restart, install bool) error {
	mode, err := mode.Get()
	if err != nil {
		return err
	}
	if mode == "install" {
		return nil
	}

	nosExists := false
	nosLocalExists := false
	if _, err := os.Stat("/sbin/nos"); err == nil {
		nosExists = true
	}
	if _, err := os.Stat("/usr/local/bin/nos"); err == nil {
		nosLocalExists = true
	}

	args := cfg.NOS.K3sArgs
	vars := []string{
		"INSTALL_K3S_NAME=service",
	}

	if !nosExists && !restart {
		return nil
	}

	if nosExists {
		vars = append(vars, "INSTALL_K3S_SKIP_DOWNLOAD=true")
		vars = append(vars, "INSTALL_K3S_BIN_DIR=/sbin")
		vars = append(vars, "INSTALL_K3S_BIN_DIR_READ_ONLY=true")
	} else if nosLocalExists {
		vars = append(vars, "INSTALL_K3S_SKIP_DOWNLOAD=true")
	} else if !install {
		return nil
	}

	if !restart {
		vars = append(vars, "INSTALL_K3S_SKIP_START=true")
	}

	if cfg.NOS.ServerURL == "" {
		if len(args) == 0 {
			args = append(args, "server")
		}
	} else {
		vars = append(vars, fmt.Sprintf("K3S_URL=%s", cfg.NOS.ServerURL))
		if len(args) == 0 {
			args = append(args, "agent")
		}
	}

	if strings.HasPrefix(cfg.NOS.Token, "K10") {
		vars = append(vars, fmt.Sprintf("K3S_TOKEN=%s", cfg.NOS.Token))
	} else if cfg.NOS.Token != "" {
		vars = append(vars, fmt.Sprintf("K3S_CLUSTER_SECRET=%s", cfg.NOS.Token))
	}

	var labels []string
	for k, v := range cfg.NOS.Labels {
		labels = append(labels, fmt.Sprintf("%s=%s", k, v))
	}
	if mode != "" {
		labels = append(labels, fmt.Sprintf("nos.io/mode=%s", mode))
	}
	labels = append(labels, fmt.Sprintf("nos.io/version=%s", version.Version))
	sort.Strings(labels)

	for _, l := range labels {
		args = append(args, "--node-label", l)
	}

	for _, taint := range cfg.NOS.Taints {
		args = append(args, "--kubelet-arg", "register-with-taints="+taint)
	}

	cmd := exec.Command("/usr/libexec/nos/nos-install.sh", args...)
	cmd.Env = append(os.Environ(), vars...)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	logrus.Debugf("Running %s %v %v", cmd.Path, cmd.Args, vars)

	return cmd.Run()
}
*/

func ApplyInstall(cfg *config.CloudConfig) error {
	mode, err := mode.Get()
	if err != nil {
		return err
	}
	if mode != "install" {
		return nil
	}

	cmd := exec.Command("nos", "install")
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	return cmd.Run()
}

// ApplyNetwork returns error
// Writes the default configuration for NetworkManager
// This can be overwritten by including a netplan-compatible configuration
// in the configuration file.
/*
func ApplyNetwork(cfg *config.CloudConfig) error {
	buf := &bytes.Buffer{}
	buf.WriteString("[main]\n")
	buf.WriteString("dns=default\n")
	buf.WriteString("plugins=keyfile\n")
	buf.WriteString("autoconnect-retries-default=0\n")
	buf.WriteString("\n")
	buf.WriteString("[keyfile]\n")
	buf.WriteString("unmanaged-devices=interface-name:balena*;interface-name:resin*;interface-name:br*;type:veth\n")
	buf.WriteString("\n")
	buf.WriteString("[connection]\n")
	buf.WriteString("ipv4.dhcp-timeout=2147483647\n")

	err := ioutil.WriteFile("/etc/NetworkManager/NetworkManager.conf", buf.Bytes(), 0644)
	if err != nil {
		return fmt.Errorf("failed to write /etc/NetworkManager/NetworkManager.conf: %v", err)
	}

	return nil
}
*/

// TODO: Remove this and make it useful for dnsmasq
func ApplyDNS(cfg *config.CloudConfig) error {
	var ifname string
	var address string

	if len(cfg.NOS.Services.DNS.InterfaceName) != 0 {
		ifname = cfg.NOS.Services.DNS.InterfaceName
	} else {
		ifname = "nos-dns"
	}
	if len(cfg.NOS.Services.DNS.InterfaceAddress) != 0 {
		address = cfg.NOS.Services.DNS.InterfaceAddress
	} else {
		address = "10.166.0.53/24"
	}

	// Create bridge and bring it up
	br, err := tenus.NewBridgeWithName(ifname)
	if err != nil {
		return err
	}

	// Parse the CIDR in to IP and subnet
	brIp, brIpNet, err := net.ParseCIDR(address)
	if err != nil {
		return err
	}

	// Set the interface IP
	if err := br.SetLinkIp(brIp, brIpNet); err != nil {
		return err
	}

	// Bring the link up
	if err = br.SetLinkUp(); err != nil {
		return err
	}

	// Get listener address
	if len(cfg.NOS.Services.DNS.InterfaceAddress) != 0 {
		// Parse the CIDR in to IP and subnet
		ipv4Addr, _, err := net.ParseCIDR(cfg.NOS.Services.DNS.InterfaceAddress)
		if err != nil {
			return err
		}
		address = ipv4Addr.String()
	} else {
		address = "10.166.0.53"
	}

	// Write DNS servers to file
	var nameservers []string
	if len(cfg.NOS.Services.DNS.NameServers) != 0 {
		nameservers = cfg.NOS.Services.DNS.NameServers
	} else {
		nameservers = append(nameservers, "8.8.8.8")
		nameservers = append(nameservers, "8.8.4.4")
	}
	buf := &bytes.Buffer{}
	for _, server := range nameservers {
		buf.WriteString("server=" + server + "\n")
	}

	err = ioutil.WriteFile("/run/dnsmasq.servers", buf.Bytes(), 0644)
	if err != nil {
		return fmt.Errorf("failed to write /run/dnsmasq.servers: %v", err)
	}

	// Create resolv.dnsmasq
	buf = &bytes.Buffer{}
	buf.WriteString("# we use dnsmasq at 127.0.0.53 so that user containers can run their own dns cache and forwarder and not conflict with dnsmasq on the host\n")
	buf.WriteString("nameserver 127.0.0.53\n")
	buf.WriteString("options timeout:15\n")

	err = ioutil.WriteFile("/etc/resolv.dnsmasq", buf.Bytes(), 0644)
	if err != nil {
		return fmt.Errorf("failed to write /etc/resolv.dnsmasq: %v", err)
	}

	// Write conf.d/dnsmasq
	buf = &bytes.Buffer{}
	buf.WriteString("DNSMASQ_OPTS=\"--user=dnsmasq --group=dnsmasq -a 127.0.0.53," + address + " -7 /etc/dnsmasq.d/ -r /etc/resolv.dnsmasq -z --servers-file=/run/dnsmasq.servers\"")
	err = ioutil.WriteFile("/etc/conf.d/dnsmasq", buf.Bytes(), 0644)
	if err != nil {
		return fmt.Errorf("failed to write /etc/conf.d/dnsmasq: %v", err)
	}
	return nil
}

// TODO: Fix this for NetworkManager
func ApplyWifi(cfg *config.CloudConfig) error {
	if len(cfg.NOS.Wifi) == 0 {
		return nil
	}

	buf := &bytes.Buffer{}

	buf.WriteString("[WiFi]\n")
	buf.WriteString("Enable=true\n")
	buf.WriteString("Tethering=false\n")

	if buf.Len() > 0 {
		if err := os.MkdirAll("/var/lib/connman", 0755); err != nil {
			return fmt.Errorf("failed to mkdir /var/lib/connman: %v", err)
		}
		if err := ioutil.WriteFile("/var/lib/connman/settings", buf.Bytes(), 0644); err != nil {
			return fmt.Errorf("failed to write to /var/lib/connman/settings: %v", err)
		}
	}

	buf = &bytes.Buffer{}

	buf.WriteString("[global]\n")
	buf.WriteString("Name=cloud-config\n")
	buf.WriteString("Description=Services defined in the cloud-config\n")

	for i, w := range cfg.NOS.Wifi {
		name := fmt.Sprintf("wifi%d", i)
		buf.WriteString("[service_")
		buf.WriteString(name)
		buf.WriteString("]\n")
		buf.WriteString("Type=wifi\n")
		buf.WriteString("Passphrase=")
		buf.WriteString(w.Passphrase)
		buf.WriteString("\n")
		buf.WriteString("Name=")
		buf.WriteString(w.Name)
		buf.WriteString("\n")
	}

	if buf.Len() > 0 {
		return ioutil.WriteFile("/var/lib/connman/cloud-config.config", buf.Bytes(), 0644)
	}

	return nil
}

func ApplyDataSource(cfg *config.CloudConfig) error {
	if len(cfg.NOS.DataSources) == 0 {
		return nil
	}

	args := strings.Join(cfg.NOS.DataSources, " ")
	buf := &bytes.Buffer{}

	buf.WriteString("command_args=\"")
	buf.WriteString(args)
	buf.WriteString("\"\n")

	if err := ioutil.WriteFile("/etc/conf.d/cloud-config", buf.Bytes(), 0644); err != nil {
		return fmt.Errorf("failed to write to /etc/conf.d/cloud-config: %v", err)
	}

	return nil
}

func ApplyEnvironment(cfg *config.CloudConfig) error {
	if len(cfg.NOS.Environment) == 0 {
		return nil
	}
	env := make(map[string]string, len(cfg.NOS.Environment))
	if buf, err := ioutil.ReadFile("/etc/environment"); err == nil {
		scanner := bufio.NewScanner(bytes.NewReader(buf))
		for scanner.Scan() {
			line := scanner.Text()
			line = strings.TrimSpace(line)
			if strings.HasPrefix(line, "#") {
				continue
			}
			line = strings.TrimPrefix(line, "export")
			line = strings.TrimSpace(line)
			if len(line) > 1 {
				parts := strings.SplitN(line, "=", 2)
				key := parts[0]
				val := ""
				if len(parts) > 1 {
					if val, err = strconv.Unquote(parts[1]); err != nil {
						val = parts[1]
					}
				}
				env[key] = val
			}
		}
	}
	for key, val := range cfg.NOS.Environment {
		env[key] = val
	}
	buf := &bytes.Buffer{}
	for key, val := range env {
		buf.WriteString(key)
		buf.WriteString("=")
		buf.WriteString(strconv.Quote(val))
		buf.WriteString("\n")
	}
	if err := ioutil.WriteFile("/etc/environment", buf.Bytes(), 0644); err != nil {
		return fmt.Errorf("failed to write to /etc/environment: %v", err)
	}

	return nil
}
