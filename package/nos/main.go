package main

// Copyright 2021 Appnostic, Inc.
// SPDX-License-Identifier: Apache-2.0

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/docker/docker/pkg/reexec"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	cmd "github.com/appnostic-io/nos/cli"
	config "github.com/appnostic-io/nos/cli/cmd/config"
	entrypoint "github.com/appnostic-io/nos/cli/cmd/entrypoint"
	rc "github.com/appnostic-io/nos/cli/cmd/rc"
	test "github.com/appnostic-io/nos/cli/cmd/test"
	"github.com/appnostic-io/nos/pkg/chroot"
)

// Opts struct
type Opts struct {
	Debug bool
}

var opts Opts

func init() {
	// initial hack to get the path of the project's bin dir
	// into the env of this cli for development
	path, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		logrus.Fatalf("FATAL: unable to get absolute bin path")
	}

	if err := os.Setenv("PATH", appendPaths(os.Getenv("PATH"), path)); err != nil {
		panic(err)
	}
	// Seed random
	rand.Seed(time.Now().UnixNano())
}

func appendPaths(envPath string, path string) string {
	if envPath == "" {
		return path
	}
	return strings.Join([]string{envPath, path}, string(os.PathListSeparator))
}

func newSigContext() (context.Context, func()) {
	ctx, cancel := context.WithCancel(context.Background())
	s := make(chan os.Signal, 1)
	signal.Notify(s, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		<-s
		cancel()
	}()
	return ctx, cancel
}

func nativeSelf(name string) string {
	// return base name
	path, err := filepath.Abs(name)
	if err != nil {
		return ""
	}
	return path
}

func main() {
	ctx, cancel := newSigContext()
	defer cancel()

	reexec.Register("/init", entrypoint.Init)      // mode=live
	reexec.Register("/sbin/init", entrypoint.Init) // mode=local
	reexec.Register("enter-root", chroot.Enter)

	if !reexec.Init() {

		if os.Geteuid() != 0 {
			fmt.Printf("%s: Need to be root\n", os.Args[0])
			os.Exit(1)
		}

		root := &cobra.Command{
			Use:   "nos",
			Short: "A command line application for nosticOS",
			Long: `nos is a command line application for the administration 
of various areas of nosticOS.`,
			SilenceErrors: true,
			SilenceUsage:  true,
			PreRunE: func(cmd *cobra.Command, args []string) error {
				if os.Geteuid() != 0 {
					fmt.Printf("%s: Need to be root\n", os.Args[0])
					os.Exit(1)
				}

				return nil
			},
			RunE: func(cmd *cobra.Command, args []string) error {
				return cmd.Help()
			},
		}

		// Add commands to the root tree
		root.AddCommand(
			config.ConfigCmd(),
			cmd.VersionCmd(),
			entrypoint.EntryCmd(),
			rc.RunLevelCmd(),
			test.TestCmd(),

			//		composeCmd.UpCmd(ctype),
			//		test.TestCmd(ctype),
		)

		helpFunc := root.HelpFunc()
		root.SetHelpFunc(func(cmd *cobra.Command, args []string) {
			helpFunc(cmd, args)
		})

		root.PersistentFlags().BoolVarP(&opts.Debug, "debug", "D", false, "Enable debug output in the logs")

		err := root.ExecuteContext(ctx)
		if err != nil {
			logrus.Fatalf("FATAL: %s", err)
		}
	}
}
