################################################################################
#
# virt-what
#
################################################################################

OPENVSWITCH_VERSION = 2.14.1
OPENVSWITCH_SOURCE = openvswitch-$(OPENVSWITCH_VERSION).tar.gz
OPENVSWITCH_SITE = https://www.openvswitch.org/releases
OPENVSWITCH_INSTALL_STAGING = YES
OPENVSWITCH_LICENSE = Apache-2.0
OPENVSWITCH_LICENSE_FILES = LICENSE
OPENVSWITCH_AUTORECONF = YES

OPENVSWITCH_TOOLS_DEPENDENCIES = host-pkgconf

define OPENVSWITCH_INSTALL_INIT_SYSV
	$(INSTALL) -D -m 0755 $(OPENVSWITCH_PKGDIR)/S20openvswitch \
		$(TARGET_DIR)/etc/init.d/S20openvswitch
endef

define OPENVSWITCH_INSTALL_DEFAULT
	$(INSTALL) -D -m 0644 $(OPENVSWITCH_PKGDIR)/openvswitch.default \
		$(TARGET_DIR)/etc/default/openvswitch
endef

OPENVSWITCH_POST_INSTALL_TARGET_HOOKS += OPENVSWITCH_INSTALL_DEFAULT

$(eval $(autotools-package))