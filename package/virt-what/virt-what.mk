################################################################################
#
# virt-what
#
################################################################################

VIRT_WHAT_VERSION = 1df728aa4b1d2814265f9c86494f7d55ee0cf9af
VIRT_WHAT_SITE = git://git.annexia.org/virt-what.git
VIRT_WHAT_SITE_METHOD = git
VIRT_WHAT_LICENSE = GPL-2.0+
VIRT_WHAT_LICENSE_FILES = COPYING
VIRT_WHAT_AUTORECONF = YES

$(eval $(autotools-package))