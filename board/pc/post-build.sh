#!/bin/bash
set -e

. $(dirname $0)/../funcs

BOARD_DIR="$(dirname $0)"
OS_NAME=nosticOS
OS_VERSION=2021.06.01
OS_ID=nosticos
OS_VERSION_ID=2021.06.01

message "Creating home directory"
mkdir -p $TARGET_DIR/home

message "Removing extraneous files and directories..."
rm -f $TARGET_DIR/etc/init.d/S80dnsmasq
rm -f $TARGET_DIR/etc/init.d/S60openvpn

message "Applying localized fixes"
touch $TARGET_DIR/etc/sysctl.conf

message "Preparing cloud-init"
pushd $TARGET_DIR/etc/runlevels/boot
ln -sf ../../init.d/cloud-init-local .
ln -sf ../../init.d/cloud-init .
ln -sf ../../init.d/cloud-config .
ln -sf ../../init.d/cloud-final .
popd

message "Preparing runlevels..."
pushd $TARGET_DIR/etc/runlevels/sysinit
for file in hwdrivers; do
  ln -sf ../../init.d/$file .
done
popd

pushd $TARGET_DIR/etc/runlevels/boot
for file in dnsmasq; do
  ln -sf ../../init.d/$file .
done
popd

pushd $TARGET_DIR/etc/runlevels/default
for file in issue; do
  ln -sf ../../init.d/$file .
done
popd

# Customize os-release
message "Writing os-release"
( \
echo "NAME=$OS_NAME"; \
echo "VERSION=$OS_VERSION"; \
echo "ID=$OS_ID"; \
echo "VERSION_ID=$OS_VERSION_ID"; \
echo "PRETTY_NAME=\"$OS_NAME $OS_VERSION\"" \
) >  $TARGET_DIR/usr/lib/os-release
