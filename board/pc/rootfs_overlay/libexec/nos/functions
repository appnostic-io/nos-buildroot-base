#!/bin/bash
set -e

export SCRIPTS=/usr/libexec/nos
export NOS_SYSTEM=/.base/sbin

reinit_debug()
{
    if [ "$NOS_DEBUG" = "true" ]; then
        set -x
        return 0
    fi

    if [ -e /run/nos/debug ]; then
        set -x
        return 0
    fi

    if [ -e /proc/cmdline ]; then
        for x in $(cat /proc/cmdline); do
            case $x in
                nos.debug*)
                    export NOS_DEBUG=true
                    mkdir -p /run/nos
                    echo debug > /run/nos/debug || true
                    ;;
            esac
        done
    fi

    if [ "$NOS_DEBUG" = "true" ]; then
        set -x
    fi
}

setup_kernel()
{
    KERNEL=${NOS_SYSTEM}/kernel/$(uname -r)/kernel.squashfs
    if [ ! -e ${KERNEL} ]; then
        return 0
    fi

    mkdir -p /run/nos/kernel
    mount -t squashfs $KERNEL /run/nos/kernel

    mount --bind /run/nos/kernel/lib/modules /lib/modules
    mount --bind /run/nos/kernel/lib/firmware /lib/firmware
    mount --bind /run/nos/kernel/headers /usr/src

    umount /run/nos/kernel
}

perr()
{
    echo "[ERROR]" "$@" 1>&2
}

pfatal()
{
    echo "[FATAL]" "$@" 1>&2
    exit 1
}

pinfo()
{
    echo " * " "$@"
}

cleanup()
{
    rm -rf /run/nos
    unset SCRIPTS
    unset NOS_SYSTEM
    if [ -n "$NOS_MODE" ]; then
        mkdir -p /run/nos
        echo "$NOS_MODE" > /run/nos/mode
    fi
    unset NOS_MODE
}

reinit_debug
